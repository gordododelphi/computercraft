local robot = require("robot")
local component = require("component")
local sides = require("sides")
local term = require("term")
local lib = require("nav")
local computer = require("computer")
local nav = component.navigation
local inv = component.inventory_controller
--local crafting = component.crafting
local waypointLookRadius = 64
local colours = {["travelling"] = 0xFFFFFF, ["waiting"] = 0x0092FF, ["FARMTWOing"] = 0x660066}
local tForward = 0
local tLeft = 0 
local tRight = 0
local tBack = 0
local FARMONE
local FARMTWO
local RECHARGER
local baseSleep = 5
local REPAIRCHEST
local cx, cy, cz
local running = true
--0 norte
--1 sul
--2 leste
--3 oeste
local switch = function(param, case_table)
    local case = case_table[param]
    if case then return case() end
    local def = case_table['default']
    return def and def() or nil
end
 function getDistanceToWaypoint(name)
    for _, v in ipairs(nav.findWaypoints(waypointLookRadius)) do
        if v.label == name then
            return v.position
        end
    end
end
function getWaypoints()
    FARMONE,RECHARGER,REPAIRCHEST,FARMTWO = {}, {}, {}, {}
  cx, cy, cz = 0, 0, 0
    local waypoints = nav.findWaypoints(waypointLookRadius)
    for i=1, waypoints.n do
        if waypoints[i].label == "FARMONE" then
        FARMONE.name = waypoints[i].label
            FARMONE.x = waypoints[i].position[1]
            FARMONE.y = waypoints[i].position[2]
            FARMONE.z = waypoints[i].position[3]
       
    elseif  waypoints[i].label == "FARMTWO" then
        FARMTWO.name = waypoints[i].label
            FARMTWO.x = waypoints[i].position[1]
            FARMTWO.y = waypoints[i].position[2]
            FARMTWO.z = waypoints[i].position[3]
        
    elseif waypoints[i].label == "RECHARGER" then
        RECHARGER.name = waypoints[i].label
            RECHARGER.x = waypoints[i].position[1]
            RECHARGER.y = waypoints[i].position[2]
            RECHARGER.z = waypoints[i].position[3]
       

       end
  
  
  --  print(waypoints[i].label)
  ----  print(" X " ..waypoints[i].position[1] )
  --  print(" Y " ..waypoints[i].position[2] )
  --  print(" Z " ..waypoints[i].position[3] )
    end
end
function getCharge()
    return computer.energy()/computer.maxEnergy()
end


function CheckSwordDamage()
 local tables = inv.getAllStacks(sides.front)
 if tables ~= nil then
for i=1, #tables do
        print(tables[i].name)
    print(tables[i].damage)
end
end
end 


function CheckoutMovement()


        local moveFunc
print("CALLING MY MOVEMENT FUNCTION")

        if direction == sides.up then
            moveFunc = lib.up
        elseif direction == sides.down then
            moveFunc = lib.down
        else
            assert(lib.sideLookup.turn[direction], "invalid direction")
            tryAction(lib.faceSide, direction)
            moveFunc = lib.forward
        end

return moveFunc()
 
end
function move(tx, ty, tz)
lib.setPosition(0,0,0,nav.getFacing())

  local dx = tx - cx
  local dy = ty - cy
  local dz = tz - cz


if lib.moveXZ(dx,dz) == true then
else
print("Algo deu  x errado.") 
local x,y,z = lib.getPosition()  
running = false
lib.setPosition(0,0,0,nav.getFacing())
CheckBattery()
running = true
init()
end



 cx, cy, cz = tx, ty, tz
end
function FARMTWOHard() 
print("Farm TWO")

--local x = getDistanceToWaypoint("FARMTWO")
move(FARMTWO.x,FARMTWO.y,FARMTWO.z)
end
function CheckBattery()
if getCharge() < 0.5 then
print("Preciso de energia, EVAAAAA!!!")
--local x = getDistanceToWaypoint("RECHARGER")
move(RECHARGER.x,RECHARGER.y,RECHARGER.z)
os.sleep(10)
end
end


function waitAtFARMONE()
print("Farm one")
--local x = getDistanceToWaypoint("FARMONE")
move(FARMONE.x,FARMONE.y,FARMONE.z)
end

function init()  
getWaypoints()
while true do

waitAtFARMONE()
FARMTWOHard()
CheckBattery()
if running == false then
break
end

end

 end
init()