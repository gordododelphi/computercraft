local port = 123
local modem = component.proxy(component.list("modem")())
local redstone = component.proxy(component.list("redstone")())


modem.open(PORT_FOLLOW)

local function findDrone()
	modem.broadcast(PORT_FOLLOW,"FOLLOW_REQUEST_LINK")
	local _,_,sender,port,_,msg = event.pull("modem_message",nil,nil,PORT_FOLLOW,nil,"FOLLOW_LINK")
	return sender
end

local drone = findDrone()

local function heartbeatHook(_,_,sender,port,_,msg)
	if sender == drone and port == PORT_FOLLOW and msg == "HEARTBEAT_REQUEST" then
		modem.send(sender,port,"HEARTBEAT_RESPONSE")
	end
end



  while true do

    local evt,_,sender,_,_,name,cmd,a,b,c = computer.pullSignal()
  if evt == "modem_message" then
  
	  if ranProperly == 1 then
	  redstone.setOutput(3,15)
	  else
	  redstone.setOutput(3,0)
	  end
  end

end